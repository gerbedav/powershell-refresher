# 
return;



# This will load the ad information for your logged in user
$User = Get-ADUser -Identity $env:USERNAME

# The formatting is default according to the amount of properties returned
# to verify this, we will select only 4 properties:
$User | Select-Object Enabled,Name,GivenName,Surname

# and with 5 properties:
$User | Select-Object Enabled,Name,GivenName,Surname,UserPrincipalName

# We can also control this ourselves
$User | Format-Table

# or
$User | Format-List

# We can also create our custom "views" for ad Users

# Note: This example has a bug in pwsh 6.2.3
$User = Get-ADUser -Identity $env:USERNAME -Properties *

# NOTE: If the above example does not work, use the workaround (PS 5 fallback):
$User = (powershell.exe -command "(Get-ADUser -Identity $env:USERNAME -Properties * | ConvertTo-Json)") | ConvertFrom-Json -AsHashtable

$User | Select-Object Enabled,Name,primarygroupid,Description,telephoneNumber


