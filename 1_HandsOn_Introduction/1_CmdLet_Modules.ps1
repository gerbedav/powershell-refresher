# To keep you from completing this task in 1s
return


# Task 1.1: See all available modules on your system
Get-Module -ListAvailable


# Task 1.2: Import the ActiveDirectory Module
Import-Module ActiveDirectory
# Notice the progressbar?

# Why do we need this?
# PS automatically loads Modules when you execute commands from the module
# Example:
Remove-Module ActiveDirectory

# The ActiveDirectory module also has a AD:\ drive
Get-PSProvider

Set-Location "AD:\OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=D,DC=ETHZ,DC=CH"

# Task 1.3: Examine the output
Get-ChildItem -Recurse

# Task 1.4: What is the difference compared to:
Get-ADObject -SearchBase "OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=D,DC=ETHZ,DC=CH" -Filter "*"

#Darstellung, zuerst Name, ObjectCLass, DistinguishedName
# bei Get-ADObject: DistinguishedName, Name, ObjectClass, ObjectGUID
